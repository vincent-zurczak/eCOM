package org.fibois38.lbb.web.rest;

import org.fibois38.lbb.BtbApp;

import org.fibois38.lbb.domain.Bois;
import org.fibois38.lbb.repository.BoisRepository;
import org.fibois38.lbb.service.BoisService;
import org.fibois38.lbb.service.dto.BoisDTO;
import org.fibois38.lbb.service.mapper.BoisMapper;
import org.fibois38.lbb.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.util.List;


import static org.fibois38.lbb.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the BoisResource REST controller.
 *
 * @see BoisResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BtbApp.class)
public class BoisResourceIntTest {

    private static final String DEFAULT_ESSENCE = "AAAAAAAAAA";
    private static final String UPDATED_ESSENCE = "BBBBBBBBBB";

    private static final byte[] DEFAULT_PHOTO = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_PHOTO = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_PHOTO_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_PHOTO_CONTENT_TYPE = "image/png";

    @Autowired
    private BoisRepository boisRepository;

    @Autowired
    private BoisMapper boisMapper;
    
    @Autowired
    private BoisService boisService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restBoisMockMvc;

    private Bois bois;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BoisResource boisResource = new BoisResource(boisService);
        this.restBoisMockMvc = MockMvcBuilders.standaloneSetup(boisResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Bois createEntity(EntityManager em) {
        Bois bois = new Bois()
            .essence(DEFAULT_ESSENCE)
            .photo(DEFAULT_PHOTO)
            .photoContentType(DEFAULT_PHOTO_CONTENT_TYPE);
        return bois;
    }

    @Before
    public void initTest() {
        bois = createEntity(em);
    }

    @Test
    @Transactional
    public void createBois() throws Exception {
        int databaseSizeBeforeCreate = boisRepository.findAll().size();

        // Create the Bois
        BoisDTO boisDTO = boisMapper.toDto(bois);
        restBoisMockMvc.perform(post("/api/bois")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(boisDTO)))
            .andExpect(status().isCreated());

        // Validate the Bois in the database
        List<Bois> boisList = boisRepository.findAll();
        assertThat(boisList).hasSize(databaseSizeBeforeCreate + 1);
        Bois testBois = boisList.get(boisList.size() - 1);
        assertThat(testBois.getEssence()).isEqualTo(DEFAULT_ESSENCE);
        assertThat(testBois.getPhoto()).isEqualTo(DEFAULT_PHOTO);
        assertThat(testBois.getPhotoContentType()).isEqualTo(DEFAULT_PHOTO_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void createBoisWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = boisRepository.findAll().size();

        // Create the Bois with an existing ID
        bois.setId(1L);
        BoisDTO boisDTO = boisMapper.toDto(bois);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBoisMockMvc.perform(post("/api/bois")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(boisDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Bois in the database
        List<Bois> boisList = boisRepository.findAll();
        assertThat(boisList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkEssenceIsRequired() throws Exception {
        int databaseSizeBeforeTest = boisRepository.findAll().size();
        // set the field null
        bois.setEssence(null);

        // Create the Bois, which fails.
        BoisDTO boisDTO = boisMapper.toDto(bois);

        restBoisMockMvc.perform(post("/api/bois")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(boisDTO)))
            .andExpect(status().isBadRequest());

        List<Bois> boisList = boisRepository.findAll();
        assertThat(boisList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBois() throws Exception {
        // Initialize the database
        boisRepository.saveAndFlush(bois);

        // Get all the boisList
        restBoisMockMvc.perform(get("/api/bois?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bois.getId().intValue())))
            .andExpect(jsonPath("$.[*].essence").value(hasItem(DEFAULT_ESSENCE.toString())))
            .andExpect(jsonPath("$.[*].photoContentType").value(hasItem(DEFAULT_PHOTO_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].photo").value(hasItem(Base64Utils.encodeToString(DEFAULT_PHOTO))));
    }
    
    @Test
    @Transactional
    public void getBois() throws Exception {
        // Initialize the database
        boisRepository.saveAndFlush(bois);

        // Get the bois
        restBoisMockMvc.perform(get("/api/bois/{id}", bois.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(bois.getId().intValue()))
            .andExpect(jsonPath("$.essence").value(DEFAULT_ESSENCE.toString()))
            .andExpect(jsonPath("$.photoContentType").value(DEFAULT_PHOTO_CONTENT_TYPE))
            .andExpect(jsonPath("$.photo").value(Base64Utils.encodeToString(DEFAULT_PHOTO)));
    }

    @Test
    @Transactional
    public void getNonExistingBois() throws Exception {
        // Get the bois
        restBoisMockMvc.perform(get("/api/bois/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBois() throws Exception {
        // Initialize the database
        boisRepository.saveAndFlush(bois);

        int databaseSizeBeforeUpdate = boisRepository.findAll().size();

        // Update the bois
        Bois updatedBois = boisRepository.findById(bois.getId()).get();
        // Disconnect from session so that the updates on updatedBois are not directly saved in db
        em.detach(updatedBois);
        updatedBois
            .essence(UPDATED_ESSENCE)
            .photo(UPDATED_PHOTO)
            .photoContentType(UPDATED_PHOTO_CONTENT_TYPE);
        BoisDTO boisDTO = boisMapper.toDto(updatedBois);

        restBoisMockMvc.perform(put("/api/bois")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(boisDTO)))
            .andExpect(status().isOk());

        // Validate the Bois in the database
        List<Bois> boisList = boisRepository.findAll();
        assertThat(boisList).hasSize(databaseSizeBeforeUpdate);
        Bois testBois = boisList.get(boisList.size() - 1);
        assertThat(testBois.getEssence()).isEqualTo(UPDATED_ESSENCE);
        assertThat(testBois.getPhoto()).isEqualTo(UPDATED_PHOTO);
        assertThat(testBois.getPhotoContentType()).isEqualTo(UPDATED_PHOTO_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingBois() throws Exception {
        int databaseSizeBeforeUpdate = boisRepository.findAll().size();

        // Create the Bois
        BoisDTO boisDTO = boisMapper.toDto(bois);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBoisMockMvc.perform(put("/api/bois")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(boisDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Bois in the database
        List<Bois> boisList = boisRepository.findAll();
        assertThat(boisList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBois() throws Exception {
        // Initialize the database
        boisRepository.saveAndFlush(bois);

        int databaseSizeBeforeDelete = boisRepository.findAll().size();

        // Get the bois
        restBoisMockMvc.perform(delete("/api/bois/{id}", bois.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Bois> boisList = boisRepository.findAll();
        assertThat(boisList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Bois.class);
        Bois bois1 = new Bois();
        bois1.setId(1L);
        Bois bois2 = new Bois();
        bois2.setId(bois1.getId());
        assertThat(bois1).isEqualTo(bois2);
        bois2.setId(2L);
        assertThat(bois1).isNotEqualTo(bois2);
        bois1.setId(null);
        assertThat(bois1).isNotEqualTo(bois2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BoisDTO.class);
        BoisDTO boisDTO1 = new BoisDTO();
        boisDTO1.setId(1L);
        BoisDTO boisDTO2 = new BoisDTO();
        assertThat(boisDTO1).isNotEqualTo(boisDTO2);
        boisDTO2.setId(boisDTO1.getId());
        assertThat(boisDTO1).isEqualTo(boisDTO2);
        boisDTO2.setId(2L);
        assertThat(boisDTO1).isNotEqualTo(boisDTO2);
        boisDTO1.setId(null);
        assertThat(boisDTO1).isNotEqualTo(boisDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(boisMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(boisMapper.fromId(null)).isNull();
    }
}
