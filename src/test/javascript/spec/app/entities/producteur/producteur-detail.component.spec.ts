/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { BtbTestModule } from '../../../test.module';
import { ProducteurDetailComponent } from 'app/entities/producteur/producteur-detail.component';
import { Producteur } from 'app/shared/model/producteur.model';

describe('Component Tests', () => {
    describe('Producteur Management Detail Component', () => {
        let comp: ProducteurDetailComponent;
        let fixture: ComponentFixture<ProducteurDetailComponent>;
        const route = ({ data: of({ producteur: new Producteur(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [BtbTestModule],
                declarations: [ProducteurDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ProducteurDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ProducteurDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.producteur).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
