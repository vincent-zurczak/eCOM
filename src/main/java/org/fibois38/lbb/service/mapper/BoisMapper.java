package org.fibois38.lbb.service.mapper;

import org.fibois38.lbb.domain.*;
import org.fibois38.lbb.service.dto.BoisDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Bois and its DTO BoisDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BoisMapper extends EntityMapper<BoisDTO, Bois> {


    @Mapping(target = "offres", ignore = true)
    Bois toEntity(BoisDTO boisDTO);

    default Bois fromId(Long id) {
        if (id == null) {
            return null;
        }
        Bois bois = new Bois();
        bois.setId(id);
        return bois;
    }
}
