package org.fibois38.lbb.service.mapper;

import org.fibois38.lbb.domain.*;
import org.fibois38.lbb.service.dto.ClientDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Client and its DTO ClientDTO.
 */
@Mapper(componentModel = "spring", uses = {CommuneMapper.class})
public interface ClientMapper extends EntityMapper<ClientDTO, Client> {


    @Mapping(target = "commandes", ignore = true)
    Client toEntity(ClientDTO clientDTO);

    default Client fromId(Long id) {
        if (id == null) {
            return null;
        }
        Client client = new Client();
        client.setId(id);
        return client;
    }
}
