package org.fibois38.lbb.domain.enumeration;

/**
 * The EtatCommande enumeration.
 */
public enum EtatCommande {
    ATTENTE, ACCEPTEE, REFUSEE, VALIDEE, EN_LIVRAISON, LIVREE
}
