package org.fibois38.lbb.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Commune.
 */
@Entity
@Table(name = "commune")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Commune implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nom", nullable = false)
    private String nom;

    @NotNull
    @Min(value = 10000)
    @Max(value = 99999)
    @Column(name = "code_postal", nullable = false)
    private Integer codePostal;

    @ManyToMany(mappedBy = "communes")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<Producteur> producteurs = new HashSet<>();

    @ManyToMany(mappedBy = "communes")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<Offre> offres = new HashSet<>();

    @ManyToMany(mappedBy = "communes")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<Client> clients = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public Commune nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Integer getCodePostal() {
        return codePostal;
    }

    public Commune codePostal(Integer codePostal) {
        this.codePostal = codePostal;
        return this;
    }

    public void setCodePostal(Integer codePostal) {
        this.codePostal = codePostal;
    }

    public Set<Producteur> getProducteurs() {
        return producteurs;
    }

    public Commune producteurs(Set<Producteur> producteurs) {
        this.producteurs = producteurs;
        return this;
    }

    public Commune addProducteur(Producteur producteur) {
        this.producteurs.add(producteur);
        producteur.getCommunes().add(this);
        return this;
    }

    public Commune removeProducteur(Producteur producteur) {
        this.producteurs.remove(producteur);
        producteur.getCommunes().remove(this);
        return this;
    }

    public void setProducteurs(Set<Producteur> producteurs) {
        this.producteurs = producteurs;
    }

    public Set<Offre> getOffres() {
        return offres;
    }

    public Commune offres(Set<Offre> offres) {
        this.offres = offres;
        return this;
    }

    public Commune addOffre(Offre offre) {
        this.offres.add(offre);
        offre.getCommunes().add(this);
        return this;
    }

    public Commune removeOffre(Offre offre) {
        this.offres.remove(offre);
        offre.getCommunes().remove(this);
        return this;
    }

    public void setOffres(Set<Offre> offres) {
        this.offres = offres;
    }

    public Set<Client> getClients() {
        return clients;
    }

    public Commune clients(Set<Client> clients) {
        this.clients = clients;
        return this;
    }

    public Commune addClient(Client client) {
        this.clients.add(client);
        client.getCommunes().add(this);
        return this;
    }

    public Commune removeClient(Client client) {
        this.clients.remove(client);
        client.getCommunes().remove(this);
        return this;
    }

    public void setClients(Set<Client> clients) {
        this.clients = clients;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Commune commune = (Commune) o;
        if (commune.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), commune.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Commune{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", codePostal=" + getCodePostal() +
            "}";
    }
}
