package org.fibois38.lbb.repository;

import org.fibois38.lbb.domain.Offre;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Offre entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OffreRepository extends JpaRepository<Offre, Long> {

    @Query(value = "select distinct offre from Offre offre left join fetch offre.communes",
        countQuery = "select count(distinct offre) from Offre offre")
    Page<Offre> findAllWithEagerRelationships(Pageable pageable);

    @Query(value = "select distinct offre from Offre offre left join fetch offre.communes")
    List<Offre> findAllWithEagerRelationships();

    @Query("select offre from Offre offre left join fetch offre.communes where offre.id =:id")
    Optional<Offre> findOneWithEagerRelationships(@Param("id") Long id);

}
