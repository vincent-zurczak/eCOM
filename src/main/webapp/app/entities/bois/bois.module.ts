import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BtbSharedModule } from 'app/shared';
import {
    BoisComponent,
    BoisDetailComponent,
    BoisUpdateComponent,
    BoisDeletePopupComponent,
    BoisDeleteDialogComponent,
    boisRoute,
    boisPopupRoute
} from './';

const ENTITY_STATES = [...boisRoute, ...boisPopupRoute];

@NgModule({
    imports: [BtbSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [BoisComponent, BoisDetailComponent, BoisUpdateComponent, BoisDeleteDialogComponent, BoisDeletePopupComponent],
    entryComponents: [BoisComponent, BoisUpdateComponent, BoisDeleteDialogComponent, BoisDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BtbBoisModule {}
