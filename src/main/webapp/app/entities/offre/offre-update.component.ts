import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { IOffre } from 'app/shared/model/offre.model';
import { OffreService } from './offre.service';
import { IBois } from 'app/shared/model/bois.model';
import { BoisService } from 'app/entities/bois';
import { ICommune } from 'app/shared/model/commune.model';
import { CommuneService } from 'app/entities/commune';
import { IProducteur } from 'app/shared/model/producteur.model';
import { ProducteurService } from 'app/entities/producteur';

@Component({
    selector: 'jhi-offre-update',
    templateUrl: './offre-update.component.html'
})
export class OffreUpdateComponent implements OnInit {
    offre: IOffre;
    isSaving: boolean;

    bois: IBois[];

    communes: ICommune[];

    producteurs: IProducteur[];
    dateLivraisonDp: any;
    datePublicationDp: any;

    constructor(
        private dataUtils: JhiDataUtils,
        private jhiAlertService: JhiAlertService,
        private offreService: OffreService,
        private boisService: BoisService,
        private communeService: CommuneService,
        private producteurService: ProducteurService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ offre }) => {
            this.offre = offre;
        });
        this.boisService.query().subscribe(
            (res: HttpResponse<IBois[]>) => {
                this.bois = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.communeService.query().subscribe(
            (res: HttpResponse<ICommune[]>) => {
                this.communes = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.producteurService.query().subscribe(
            (res: HttpResponse<IProducteur[]>) => {
                this.producteurs = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.offre.id !== undefined) {
            this.subscribeToSaveResponse(this.offreService.update(this.offre));
        } else {
            this.subscribeToSaveResponse(this.offreService.create(this.offre));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IOffre>>) {
        result.subscribe((res: HttpResponse<IOffre>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackBoisById(index: number, item: IBois) {
        return item.id;
    }

    trackCommuneById(index: number, item: ICommune) {
        return item.id;
    }

    trackProducteurById(index: number, item: IProducteur) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}
