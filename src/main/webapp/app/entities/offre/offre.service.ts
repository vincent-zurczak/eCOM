import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IOffre } from 'app/shared/model/offre.model';

type EntityResponseType = HttpResponse<IOffre>;
type EntityArrayResponseType = HttpResponse<IOffre[]>;

@Injectable({ providedIn: 'root' })
export class OffreService {
    private resourceUrl = SERVER_API_URL + 'api/offres';

    constructor(private http: HttpClient) {}

    create(offre: IOffre): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(offre);
        return this.http
            .post<IOffre>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(offre: IOffre): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(offre);
        return this.http
            .put<IOffre>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IOffre>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IOffre[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(offre: IOffre): IOffre {
        const copy: IOffre = Object.assign({}, offre, {
            dateLivraison: offre.dateLivraison != null && offre.dateLivraison.isValid() ? offre.dateLivraison.format(DATE_FORMAT) : null,
            datePublication:
                offre.datePublication != null && offre.datePublication.isValid() ? offre.datePublication.format(DATE_FORMAT) : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.dateLivraison = res.body.dateLivraison != null ? moment(res.body.dateLivraison) : null;
        res.body.datePublication = res.body.datePublication != null ? moment(res.body.datePublication) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((offre: IOffre) => {
            offre.dateLivraison = offre.dateLivraison != null ? moment(offre.dateLivraison) : null;
            offre.datePublication = offre.datePublication != null ? moment(offre.datePublication) : null;
        });
        return res;
    }
}
