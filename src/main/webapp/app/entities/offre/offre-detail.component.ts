import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IOffre } from 'app/shared/model/offre.model';

@Component({
    selector: 'jhi-offre-detail',
    templateUrl: './offre-detail.component.html'
})
export class OffreDetailComponent implements OnInit {
    offre: IOffre;

    constructor(private dataUtils: JhiDataUtils, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ offre }) => {
            this.offre = offre;
        });
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }
}
