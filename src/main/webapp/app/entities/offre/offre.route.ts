import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Offre } from 'app/shared/model/offre.model';
import { OffreService } from './offre.service';
import { OffreComponent } from './offre.component';
import { OffreDetailComponent } from './offre-detail.component';
import { OffreUpdateComponent } from './offre-update.component';
import { OffreDeletePopupComponent } from './offre-delete-dialog.component';
import { IOffre } from 'app/shared/model/offre.model';

@Injectable({ providedIn: 'root' })
export class OffreResolve implements Resolve<IOffre> {
    constructor(private service: OffreService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((offre: HttpResponse<Offre>) => offre.body));
        }
        return of(new Offre());
    }
}

export const offreRoute: Routes = [
    {
        path: 'offre',
        component: OffreComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'btbApp.offre.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'offre/:id/view',
        component: OffreDetailComponent,
        resolve: {
            offre: OffreResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'btbApp.offre.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'offre/new',
        component: OffreUpdateComponent,
        resolve: {
            offre: OffreResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'btbApp.offre.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'offre/:id/edit',
        component: OffreUpdateComponent,
        resolve: {
            offre: OffreResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'btbApp.offre.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const offrePopupRoute: Routes = [
    {
        path: 'offre/:id/delete',
        component: OffreDeletePopupComponent,
        resolve: {
            offre: OffreResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'btbApp.offre.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
