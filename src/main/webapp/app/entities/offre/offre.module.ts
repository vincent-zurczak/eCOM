import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BtbSharedModule } from 'app/shared';
import {
    OffreComponent,
    OffreDetailComponent,
    OffreUpdateComponent,
    OffreDeletePopupComponent,
    OffreDeleteDialogComponent,
    offreRoute,
    offrePopupRoute
} from './';

const ENTITY_STATES = [...offreRoute, ...offrePopupRoute];

@NgModule({
    imports: [BtbSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [OffreComponent, OffreDetailComponent, OffreUpdateComponent, OffreDeleteDialogComponent, OffreDeletePopupComponent],
    entryComponents: [OffreComponent, OffreUpdateComponent, OffreDeleteDialogComponent, OffreDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BtbOffreModule {}
