import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { ICommune } from 'app/shared/model/commune.model';
import { CommuneService } from './commune.service';
import { IProducteur } from 'app/shared/model/producteur.model';
import { ProducteurService } from 'app/entities/producteur';
import { IOffre } from 'app/shared/model/offre.model';
import { OffreService } from 'app/entities/offre';
import { IClient } from 'app/shared/model/client.model';
import { ClientService } from 'app/entities/client';

@Component({
    selector: 'jhi-commune-update',
    templateUrl: './commune-update.component.html'
})
export class CommuneUpdateComponent implements OnInit {
    commune: ICommune;
    isSaving: boolean;

    producteurs: IProducteur[];

    offres: IOffre[];

    clients: IClient[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private communeService: CommuneService,
        private producteurService: ProducteurService,
        private offreService: OffreService,
        private clientService: ClientService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ commune }) => {
            this.commune = commune;
        });
        this.producteurService.query().subscribe(
            (res: HttpResponse<IProducteur[]>) => {
                this.producteurs = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.offreService.query().subscribe(
            (res: HttpResponse<IOffre[]>) => {
                this.offres = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.clientService.query().subscribe(
            (res: HttpResponse<IClient[]>) => {
                this.clients = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.commune.id !== undefined) {
            this.subscribeToSaveResponse(this.communeService.update(this.commune));
        } else {
            this.subscribeToSaveResponse(this.communeService.create(this.commune));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ICommune>>) {
        result.subscribe((res: HttpResponse<ICommune>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackProducteurById(index: number, item: IProducteur) {
        return item.id;
    }

    trackOffreById(index: number, item: IOffre) {
        return item.id;
    }

    trackClientById(index: number, item: IClient) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}
