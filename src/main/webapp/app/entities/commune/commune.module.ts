import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BtbSharedModule } from 'app/shared';
import {
    CommuneComponent,
    CommuneDetailComponent,
    CommuneUpdateComponent,
    CommuneDeletePopupComponent,
    CommuneDeleteDialogComponent,
    communeRoute,
    communePopupRoute
} from './';

const ENTITY_STATES = [...communeRoute, ...communePopupRoute];

@NgModule({
    imports: [BtbSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        CommuneComponent,
        CommuneDetailComponent,
        CommuneUpdateComponent,
        CommuneDeleteDialogComponent,
        CommuneDeletePopupComponent
    ],
    entryComponents: [CommuneComponent, CommuneUpdateComponent, CommuneDeleteDialogComponent, CommuneDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BtbCommuneModule {}
