import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Producteur } from 'app/shared/model/producteur.model';
import { ProducteurService } from './producteur.service';
import { ProducteurComponent } from './producteur.component';
import { ProducteurDetailComponent } from './producteur-detail.component';
import { ProducteurUpdateComponent } from './producteur-update.component';
import { ProducteurDeletePopupComponent } from './producteur-delete-dialog.component';
import { IProducteur } from 'app/shared/model/producteur.model';

@Injectable({ providedIn: 'root' })
export class ProducteurResolve implements Resolve<IProducteur> {
    constructor(private service: ProducteurService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((producteur: HttpResponse<Producteur>) => producteur.body));
        }
        return of(new Producteur());
    }
}

export const producteurRoute: Routes = [
    {
        path: 'producteur',
        component: ProducteurComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'btbApp.producteur.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'producteur/:id/view',
        component: ProducteurDetailComponent,
        resolve: {
            producteur: ProducteurResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'btbApp.producteur.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'producteur/new',
        component: ProducteurUpdateComponent,
        resolve: {
            producteur: ProducteurResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'btbApp.producteur.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'producteur/:id/edit',
        component: ProducteurUpdateComponent,
        resolve: {
            producteur: ProducteurResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'btbApp.producteur.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const producteurPopupRoute: Routes = [
    {
        path: 'producteur/:id/delete',
        component: ProducteurDeletePopupComponent,
        resolve: {
            producteur: ProducteurResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'btbApp.producteur.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
