import { IOffre } from 'app/shared/model//offre.model';
import { ICommune } from 'app/shared/model//commune.model';

export interface IProducteur {
    id?: number;
    email?: string;
    photoContentType?: string;
    photo?: any;
    nom?: string;
    prenom?: string;
    password?: any;
    description?: any;
    offres?: IOffre[];
    communes?: ICommune[];
}

export class Producteur implements IProducteur {
    constructor(
        public id?: number,
        public email?: string,
        public photoContentType?: string,
        public photo?: any,
        public nom?: string,
        public prenom?: string,
        public password?: any,
        public description?: any,
        public offres?: IOffre[],
        public communes?: ICommune[]
    ) {}
}
