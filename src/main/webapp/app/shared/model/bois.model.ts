import { IOffre } from 'app/shared/model//offre.model';

export interface IBois {
    id?: number;
    essence?: string;
    photoContentType?: string;
    photo?: any;
    offres?: IOffre[];
}

export class Bois implements IBois {
    constructor(
        public id?: number,
        public essence?: string,
        public photoContentType?: string,
        public photo?: any,
        public offres?: IOffre[]
    ) {}
}
