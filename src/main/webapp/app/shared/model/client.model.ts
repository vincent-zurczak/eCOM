import { ICommune } from 'app/shared/model//commune.model';
import { ICommande } from 'app/shared/model//commande.model';

export interface IClient {
    id?: number;
    email?: string;
    photoContentType?: string;
    photo?: any;
    nom?: string;
    prenom?: string;
    password?: any;
    communes?: ICommune[];
    commandes?: ICommande[];
}

export class Client implements IClient {
    constructor(
        public id?: number,
        public email?: string,
        public photoContentType?: string,
        public photo?: any,
        public nom?: string,
        public prenom?: string,
        public password?: any,
        public communes?: ICommune[],
        public commandes?: ICommande[]
    ) {}
}
