import { Moment } from 'moment';
import { ICommune } from 'app/shared/model//commune.model';
import { ICommande } from 'app/shared/model//commande.model';

export const enum Frequence {
    PONCTUEL = 'PONCTUEL',
    QUOTIDIEN = 'QUOTIDIEN',
    HEBDOMADAIRE = 'HEBDOMADAIRE',
    MENSUEL = 'MENSUEL'
}

export interface IOffre {
    id?: number;
    nom?: string;
    prix?: number;
    taille?: number;
    frequence?: Frequence;
    description?: any;
    stock?: number;
    dateLivraison?: Moment;
    datePublication?: Moment;
    boisId?: number;
    communes?: ICommune[];
    producteurId?: number;
    commandes?: ICommande[];
}

export class Offre implements IOffre {
    constructor(
        public id?: number,
        public nom?: string,
        public prix?: number,
        public taille?: number,
        public frequence?: Frequence,
        public description?: any,
        public stock?: number,
        public dateLivraison?: Moment,
        public datePublication?: Moment,
        public boisId?: number,
        public communes?: ICommune[],
        public producteurId?: number,
        public commandes?: ICommande[]
    ) {}
}
