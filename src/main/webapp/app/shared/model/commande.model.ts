import { Moment } from 'moment';

export const enum EtatCommande {
    ATTENTE = 'ATTENTE',
    ACCEPTEE = 'ACCEPTEE',
    REFUSEE = 'REFUSEE',
    VALIDEE = 'VALIDEE',
    EN_LIVRAISON = 'EN_LIVRAISON',
    LIVREE = 'LIVREE'
}

export interface ICommande {
    id?: number;
    date?: Moment;
    prix?: number;
    etat?: EtatCommande;
    offreId?: number;
    clientId?: number;
}

export class Commande implements ICommande {
    constructor(
        public id?: number,
        public date?: Moment,
        public prix?: number,
        public etat?: EtatCommande,
        public offreId?: number,
        public clientId?: number
    ) {}
}
